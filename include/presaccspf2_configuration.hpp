//
// Copyright (c) 2017-2020 Santini Designs. All rights reserved.
//

#pragma once

#include <basic/types.hpp>

#include <eye/protocol.hpp>

namespace user_tasks::presaccspf2 {

/**
 * The welcome banner configuration class is configuration class for the welcome banner task.
 */
struct presaccspf2Configuration : public eye::protocol::EyerisTaskConfiguration
{
    using ptr_t = std::shared_ptr<presaccspf2Configuration>;  ///< Pointer type definition for the class

    /**
     * @brief Static factory.
     *
     * @param[in] other Pointer to another JSON object that defines the initial schema of this one.
     *
     * @return Pointer to a new class instance
     */
    [[maybe_unused]]
    static ptr_t factory_ptr(const basic::types::JSON::sptr_t& other)
    {
        return std::make_shared<presaccspf2Configuration>(other);
    }

    /**
     * @brief Constructor that instantiate the configuration from the prototype, but then let set the values
     * also based on the parent configuration.
     *
     * @param[in] other Pointer to a JsonObject used to initialize the configuration
     */
    explicit presaccspf2Configuration(basic::types::JSON::sptr_t other) :
        EyerisTaskConfiguration(other)
    {
        initializeimgStep(30);
        initializeOptoEcc(13);
        initializeOptoSize(5);
        initializeOptoBeep(0); // proportion of optotype trials out of trial lim that ask for different response
        initializeOptoFlag(0); // proportion of optotype trials out of trial lim that show the stimuli
        initializeAmplitude(1.0f); // controls difficulty
        initializeGaborSigma(7.0f); // size of gabor standard deviation
        initializeFixSize(6); // in arcmin
        initializetiltdeg(45); // in degrees
        initializegaborEcc(30); // in arcmin
        initializespf_id(4.0f); // 0,1,2,3
        initializesaccVelThreshold(4.5f);
        initializeminTmThres(150.0f);
        initializenTrialLim(200.0f);
        initializemaxTmThres(900.0f);
        initializepestFlag(0.0f);
        initializenTrialLim(100.0f);
        initializenBefore(100);
        initializenAfter(100);
        initializeSeed(600.0f);

    }
    LC_PROPERTY_FLOAT(Seed, "LatencySeed", Seed);
    LC_PROPERTY_INT(IMGSTEP, "imgStep", imgStep);
    LC_PROPERTY_INT(OPTOSIZE, "optoSize", OptoSize);
    LC_PROPERTY_INT(OPTOECC, "optoEcc", OptoEcc);
    LC_PROPERTY_INT(OPTOBEEP, "optoBeep", OptoBeep);
    LC_PROPERTY_INT(OPTOFLAG, "optoFlag", OptoFlag);
    LC_PROPERTY_FLOAT(PESTFLAG,"Pest condition",pestFlag);
    LC_PROPERTY_FLOAT(AMPLITUDE, "amplitude", Amplitude);
    LC_PROPERTY_FLOAT(GABORSIGMA, "gaborSigma", GaborSigma);
    LC_PROPERTY_FLOAT(FIXSIZE,"fixSize",FixSize);
    LC_PROPERTY_FLOAT(TILTDEG,"tiltDeg",tiltdeg);
    LC_PROPERTY_FLOAT(GABORECC,"gaborEcc",gaborEcc);
    LC_PROPERTY_FLOAT(SPF,"spf_id",spf_id);
    LC_PROPERTY_FLOAT(SACCVELTHRES,"saccVelThreshold",saccVelThreshold);
    LC_PROPERTY_FLOAT(MINTMTHRES,"minTmThres",minTmThres);
    LC_PROPERTY_FLOAT(NTRIALLIM,"nTrialLim",nTrialLim);
    LC_PROPERTY_INT(NBEFORE,"nBefore",nBefore);
    LC_PROPERTY_INT(NAFTER,"nAfter",nAfter);
    LC_PROPERTY_FLOAT(MAXTMTHRES,"maxTmThres",maxTmThres);

};

}  // namespace user_tasks::presaccspf2
